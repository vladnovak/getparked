alter table queueandreservationhistory
    drop constraint queueandreservationhistory_driverid_fkey;
alter table queueandreservationhistory
    add constraint queueandreservationhistory_driverid_fkey
        foreign key (personid) references person(id)
            on delete cascade;

alter table queueandreservationhistory
    drop constraint queueandreservationhistory_parkinglotid_fkey;
alter table queueandreservationhistory
    add constraint queueandreservationhistory_parkinglotid_fkey
        foreign key (parkinglotid) references parkinglot(id)
            on delete cascade;