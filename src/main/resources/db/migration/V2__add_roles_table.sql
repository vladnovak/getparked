ALTER TABLE driver RENAME TO Person;

ALTER TABLE queue RENAME COLUMN driverid TO personId;

ALTER TABLE reservation RENAME COLUMN driverid TO personId;

ALTER TABLE queueandreservationhistory RENAME COLUMN driverid TO personId;

CREATE TABLE IF NOT EXISTS Role (
    id SERIAL NOT NULL PRIMARY KEY,
    roleEnum VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS person_role (
    person_id INT NOT NULL REFERENCES Person(id) ON DELETE CASCADE,
    role_id INT NOT NULL REFERENCES Role(id) ON DELETE RESTRICT,
    PRIMARY KEY (person_id, role_id)
);