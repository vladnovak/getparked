delete from reservation;
delete from queue;

INSERT INTO person(id, firstname, lastname, email, telegramid, licenseplate, login, password)
VALUES (1, 'John', 'King', 'john@yahoo.com', '@johnk', 'WY777VV','john', '1q2w3e'),
       (2, 'Денис', 'Іванов', 'denys@gmail.com', '@dennn', 'WW11122', 'denys', '1q2w3e'),
       (3, 'Władysław', 'Brzęczyszczykiewicz', 'wladek@wp.pl', '@wladekb', 'AA1F135VV', 'wladekb', '1q2w3e'),
       (4, 'Jürgen', 'Günther', 'jurgen@gmx.de', null, 'MBD147', 'jurgen.gunther', '1q2w3e')
ON CONFLICT DO NOTHING;

insert into parkinglot(id, blockid, lotnumber, available, owner)
values (1, 'b1', 1, true, 1) on conflict do nothing;

insert into parkinglot(id, blockid, lotnumber, available, owner)
values (2, 'b2', 2, true, 1) on conflict do nothing;

insert into parkinglot(id, blockid, lotnumber, available)
values (3, 'b3', 3, true) on conflict do nothing;

insert into parkinglot(id, blockid, lotnumber, available)
values (4, 'b4', 4, true) on conflict do nothing;

update person set assignedlotid=1 where id=1;
update person set assignedlotid=2 where id=2;

INSERT INTO role(roleenum) VALUES ('ROLE_DRIVER'), ('ROLE_ADMIN') ON CONFLICT DO NOTHING;

INSERT INTO person_role(person_id, role_id) VALUES (1, 1), (1, 2), (2, 1), (3, 1) ON CONFLICT DO NOTHING;
