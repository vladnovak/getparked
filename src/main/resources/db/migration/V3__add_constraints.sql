ALTER TABLE parkinglot ADD CONSTRAINT block_lot UNIQUE (blockid, lotnumber);

ALTER TABLE role ADD CONSTRAINT role_name UNIQUE (roleenum);

ALTER TABLE queue ADD CONSTRAINT unique_person_id UNIQUE (personid);