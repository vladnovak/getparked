package pl.novak.GetParked.controller;

import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import pl.novak.GetParked.model.Person;
import pl.novak.GetParked.service.ParkingService;

@Controller
@AllArgsConstructor
@Secured({"ROLE_ADMIN", "ROLE_DRIVER"})
public class ParkingController {
    private final ParkingService parkingService;

    @PostMapping("/request")
    public String requestReservation(@AuthenticationPrincipal Person person) {
        parkingService.processRequest(person);
        return "redirect:home";
    }

    @PostMapping("/cancel")
    public String handleCancellation(@AuthenticationPrincipal Person person) {
        parkingService.cancelParkingAndDistributeFreeLot(person.getId());
        return "redirect:home";
    }
}
