package pl.novak.GetParked.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Secured("ROLE_ADMIN")
public class AdminController {

    @GetMapping("/admin")
    public ModelAndView admin() {
        return new ModelAndView("admin_page");
    }
}
