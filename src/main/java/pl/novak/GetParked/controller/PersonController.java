package pl.novak.GetParked.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.novak.GetParked.model.Person;
import pl.novak.GetParked.service.ParkingService;

@Controller
@RequiredArgsConstructor
@Secured({"ROLE_DRIVER", "ROLE_ADMIN"})
public class PersonController {
    private final ParkingService parkingService;

    @GetMapping("/home")
    public ModelAndView defaultPage(@AuthenticationPrincipal Person person) {
        return new ModelAndView("driver_page")
                .addObject("currentParkingSituationForTheUser", parkingService.buildDTO(person));
    }
}
