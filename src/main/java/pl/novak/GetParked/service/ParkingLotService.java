package pl.novak.GetParked.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.novak.GetParked.model.ParkingLot;
import pl.novak.GetParked.model.Person;
import pl.novak.GetParked.repository.ParkingLotRepository;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Service
@AllArgsConstructor
public class ParkingLotService {
    private final ParkingLotRepository parkingLotRepository;

    public long countAvailableLots(boolean includeAssignedLots) {
        if (includeAssignedLots) {
            return parkingLotRepository
                    .countAllByAvailableTrueAndCurrentReservationIsNull();
        } else return parkingLotRepository
                .countAllByAvailableTrueAndCurrentReservationIsNullAndOwnerIsNull();
    }

    public Optional<ParkingLot> findFirstAvailableLot(Person person, boolean includeAssignedLots) {
        return includeAssignedLots ? findAssignedLot(person) : findGenericLot();
    }

    private Optional<ParkingLot> findAssignedLot(Person person) {
        return nonNull(person.getAssignedLot())
                ? parkingLotRepository
                .findByOwnerIdAndAvailableTrueAndCurrentReservationIsNull(person.getId())
                : parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNull();
    }

    private Optional<ParkingLot> findGenericLot() {
        return parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNullAndOwnerIsNull();
    }


    public List<ParkingLot> findAll() {
        return parkingLotRepository.findAll();
    }
}
