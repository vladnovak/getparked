package pl.novak.GetParked.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.novak.GetParked.model.*;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.IntStream;

import static java.time.LocalTime.now;

@Service
@RequiredArgsConstructor
@Slf4j
public class ParkingService {
    private final QueueService queueService;
    private final ReservationService reservationService;
    private final ParkingLotService parkingLotService;
    @Value("${assignedLotsHour.start}")
    private LocalTime startAssignedLotsHour;
    @Value("${assignedLotsHour.end}")
    private LocalTime endAssignedLotsHour;

    private boolean includeAssignedLots() {
        return now().isBefore(startAssignedLotsHour) && now().isAfter(endAssignedLotsHour);
    }

    public DriverPageDTO buildDTO(Person person) {
        var dto = new DriverPageDTO();
        dto.setPerson(person);
        var bookingDate = DateTimeFormatter.ofPattern("dd MMMM yyyy")
                .format(LocalDateTime.now().plusDays(1));
        dto.setBookingDate(bookingDate);
        reservationService.findByPersonId(person.getId())
                .ifPresentOrElse(
                        dto::setReservation,
                        () -> queueService.findByPersonId(person.getId())
                                .ifPresentOrElse(
                                        queue -> dto.setPlaceInQueue(queueService
                                                .getCurrentPositionInQueue(queue)),
                                        () -> {
                                            dto
                                                    .setAvailableLotsNumber(parkingLotService
                                                            .countAvailableLots(includeAssignedLots()));
                                            dto.setQueueSize(queueService.getQueueSize());
                                        }));
        return dto;
    }

    public void processRequest(Person person) {
        parkingLotService.findFirstAvailableLot(person, includeAssignedLots())
                .ifPresentOrElse(
                        parkingLot -> createReservation(person, parkingLot),
                        () -> putInQueue(person)
                );
    }

    private void createReservation(Person person, ParkingLot parkingLot) {
        var persistedReservation = reservationService
                .save(Reservation.builder()
                        .person(person)
                        .parkingLot(parkingLot)
                        .build());
        parkingLot.setCurrentReservation(persistedReservation);
        log.info("Created : " + persistedReservation);
    }

    private void putInQueue(Person person) {
        var persistedQueue = queueService
                .save(Queue.builder().person(person).build());
        log.info("Created : " + persistedQueue);
    }

    @Transactional
    public void cancelParkingAndDistributeFreeLot(Long personId) {
        queueService.findByPersonId(personId)
                .ifPresentOrElse(
                        queue -> queueService.deleteByPersonId(personId),
                        () -> {
                            reservationService.cancelByPersonId(personId);
                            distributeAvailableLots();
                        });
    }

    @Transactional
    public void distributeAvailableLots() {
        var queues = queueService.findAll();
        var availableParkingLots = parkingLotService.findAll();
        var driversInQueueAmount = queues.size();
        var availableSlotsAmount = availableParkingLots.size();
        var slotsToDistribute = Integer.min(driversInQueueAmount, availableSlotsAmount);

        IntStream.range(0, slotsToDistribute)
                .forEach(index -> {
                    var queue = queues.get(index);
                    var parkingLot = availableParkingLots.get(index);
                    createReservation(queue.getPerson(), parkingLot);
                    queueService.deleteById(queue.getId());
                });
    }
}
