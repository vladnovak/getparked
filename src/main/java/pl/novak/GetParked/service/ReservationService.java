package pl.novak.GetParked.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.novak.GetParked.model.Reservation;
import pl.novak.GetParked.repository.ReservationRepository;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ReservationService {
    private final ReservationRepository reservationRepository;

    public Optional<Reservation> findByPersonId(Long personId) {
        return reservationRepository.findReservationByPersonId(personId);
    }

    @Transactional
    public void cancelByPersonId(Long personId) {
        reservationRepository.deleteReservationByPersonId(personId);
    }

    @Transactional
    public void deleteAll() {
        reservationRepository.deleteAll();
    }

    public Reservation save(Reservation reservation) {
        return reservationRepository.save(reservation);
    }
}
