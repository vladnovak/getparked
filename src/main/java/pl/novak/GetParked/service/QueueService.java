package pl.novak.GetParked.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pl.novak.GetParked.model.Queue;
import pl.novak.GetParked.repository.QueueRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@EnableTransactionManagement
public class QueueService {
    private final QueueRepository queueRepository;

    public List<Queue> findAll() {
        return queueRepository.findAllByOrderByIdAsc();
    }

    public Optional<Queue> findByPersonId(Long personId) {
        return queueRepository.findByPersonId(personId);
    }

    public Integer getCurrentPositionInQueue(Queue queue) {
        var index = queueRepository
                .findAllByOrderByIdAsc()
                .indexOf(queue);
        return ++index;
    }

    public long getQueueSize() {
        return queueRepository.count();
    }

    public Queue save(Queue queue) {
        return queueRepository.save(queue);
    }

    public Optional<Queue> findFirst() {
        return queueRepository.findFirstByOrderByIdAsc();
    }

    public void deleteByPersonId(Long id) {
        queueRepository.deleteByPersonId(id);
    }

    public void deleteAll() {
        queueRepository.deleteAll();
    }

    public void deleteById(Long id) {
        queueRepository.deleteById(id);
    }
}
