package pl.novak.GetParked.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;
import pl.novak.GetParked.model.Person;
import pl.novak.GetParked.repository.PersonRepository;

import java.util.List;

@Service
@AllArgsConstructor
@Data
public class PersonService {
    private PersonRepository personRepository;

    public List<Person> findAll() {
        return personRepository.findAll();
    }
}
