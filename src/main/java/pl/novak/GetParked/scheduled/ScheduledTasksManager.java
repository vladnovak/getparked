package pl.novak.GetParked.scheduled;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.novak.GetParked.service.ParkingService;
import pl.novak.GetParked.service.QueueService;
import pl.novak.GetParked.service.ReservationService;

@Component
@Slf4j
@AllArgsConstructor
public class ScheduledTasksManager {
    private final ReservationService reservationService;
    private final QueueService queueService;
    private final ParkingService parkingService;

    @Scheduled(cron = "${cronValues.cleanUpTime:-}")
    public void cleanUpAfterPrevSession() {
        log.info("Scheduled job cleanUpAfterPrevSession started.");
        reservationService.deleteAll();
        queueService.deleteAll();
        log.info("Schedule job cleanUpAfterPrevSession successfully finished.");
    }

    @Scheduled(cron = "${cronValues.distributeAvailableLotsTime:-}")
    public void distributeAvailableLots() {
        parkingService.distributeAvailableLots();
        log.info("An attempt of distribution available lots has been made");
    }
}
