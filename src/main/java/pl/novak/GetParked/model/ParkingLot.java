package pl.novak.GetParked.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "ParkingLot")
public class ParkingLot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String blockId;
    private Integer lotNumber;
    private Boolean available;
    private LocalDate unavailableFrom;
    private LocalDate unavailableTo;
    @OneToOne(mappedBy = "assignedLot")
    private Person owner;
    @OneToOne(mappedBy = "parkingLot")
    private Reservation currentReservation;

    @Override
    public String toString() {
        return "ParkingLot{" +
                "id=" + id +
                ", blockId='" + blockId + '\'' +
                ", lotNumber=" + lotNumber +
                ", owner=" + owner +
                '}';
    }
}
