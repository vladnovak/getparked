package pl.novak.GetParked.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Data
@NoArgsConstructor
public class Role implements GrantedAuthority {
    public enum RoleEnum {
        ROLE_DRIVER, ROLE_ADMIN
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private RoleEnum roleEnum;
    @Override
    public String getAuthority() {
        return roleEnum.name();
    }
}
