package pl.novak.GetParked.model;

import lombok.Data;

@Data
public class DriverPageDTO {
    private Person person;
    private String bookingDate;
    private Reservation reservation;
    private Long availableLotsNumber;
    private Long queueSize;
    private Integer placeInQueue;
}
