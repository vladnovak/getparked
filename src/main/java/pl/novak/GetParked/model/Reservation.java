package pl.novak.GetParked.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "Reservation")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @OneToOne
    @JoinColumn(name = "parkinglotid", referencedColumnName = "id")
    private ParkingLot parkingLot;
    @OneToOne
    @JoinColumn(name = "personid", referencedColumnName = "id")
    private Person person;

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", parkingLot=" + parkingLot +
                ", person=" + person +
                '}';
    }
}
