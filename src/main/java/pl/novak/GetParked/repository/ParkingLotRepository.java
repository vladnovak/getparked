package pl.novak.GetParked.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.novak.GetParked.model.ParkingLot;
import pl.novak.GetParked.model.Person;

import java.util.Optional;

@Repository
public interface ParkingLotRepository extends JpaRepository<ParkingLot, Long> {
    Optional<ParkingLot> findFirstByAvailableTrueAndCurrentReservationIsNull();
    Optional<ParkingLot> findByOwnerIdAndAvailableTrueAndCurrentReservationIsNull(Long ownerId);

    Optional<ParkingLot> findFirstByAvailableTrueAndCurrentReservationIsNullAndOwnerIsNull();
    long countAllByAvailableTrueAndCurrentReservationIsNullAndOwnerIsNull();

    long countAllByAvailableTrueAndCurrentReservationIsNull();
}
