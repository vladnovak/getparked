package pl.novak.GetParked.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.novak.GetParked.model.Reservation;

import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    Optional<Reservation> findReservationByPersonId(Long personId);

    void deleteReservationByPersonId(Long personId);
}
