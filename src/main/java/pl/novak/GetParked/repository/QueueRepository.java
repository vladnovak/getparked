package pl.novak.GetParked.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.novak.GetParked.model.Queue;

import java.util.List;
import java.util.Optional;

@Repository
public interface QueueRepository extends JpaRepository<Queue, Long> {
    Optional<Queue> findByPersonId(Long personId);
    long count();
    void deleteByPersonId(Long personId);
    void deleteById(Long id);
    Optional<Queue> findFirstByOrderByIdAsc();
    List<Queue> findAllByOrderByIdAsc();
}
