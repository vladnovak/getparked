package pl.novak.GetParked;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GetParkedApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetParkedApplication.class, args);
	}

}
