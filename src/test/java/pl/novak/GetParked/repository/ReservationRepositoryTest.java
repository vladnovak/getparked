package pl.novak.GetParked.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;
import pl.novak.GetParked.model.ParkingLot;
import pl.novak.GetParked.model.Person;
import pl.novak.GetParked.model.Reservation;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
@Sql(statements = "DELETE FROM queue", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class ReservationRepositoryTest {
    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    ParkingLotRepository parkingLotRepository;
    Person person1;
    ParkingLot parkingLot1;

    @BeforeEach
    void initializeVariables() {
        person1 = Person.builder()
                .firstName("person1")
                .lastName("lastname1")
                .email("email1")
                .login("login1")
                .password("pass1")
                .banned(false)
                .build();
        parkingLot1 = ParkingLot.builder()
                .blockId("b1")
                .lotNumber(1)
                .available(true)
                .build();
    }

    @Test
    @DisplayName("Should return an optional of Reservation by personId")
    void findReservationByPersonId() {
        var persistedPerson1 = personRepository.save(person1);
        var persistedParkingLot1 = parkingLotRepository.save(parkingLot1);
        var reservation = Reservation.builder()
                .person(persistedPerson1)
                .parkingLot(persistedParkingLot1)
                .build();

        assertTrue(reservationRepository
                .findReservationByPersonId(persistedPerson1.getId())
                .isEmpty());

        var persistedReservation = reservationRepository.save(reservation);
        var reservationOptByPersonId = reservationRepository
                .findReservationByPersonId(persistedPerson1.getId());

        assertAll(
                () -> assertNotNull(persistedReservation.getId()),
                () -> assertTrue(reservationOptByPersonId.isPresent()),
                () -> assertEquals(reservationOptByPersonId.get(), persistedReservation),
                () -> assertTrue(reservationRepository
                        .findReservationByPersonId(persistedPerson1.getId() + 1L)
                        .isEmpty())
        );
    }

    @Test
    @DisplayName("Should delete existing Reservation by personId")
    void deleteReservationByPersonId() {
        var persistedPerson1 = personRepository.save(person1);
        var persistedParkingLot1 = parkingLotRepository.save(parkingLot1);
        var reservation = Reservation.builder()
                .person(persistedPerson1)
                .parkingLot(persistedParkingLot1)
                .build();

        assertTrue(reservationRepository
                .findReservationByPersonId(persistedPerson1.getId())
                .isEmpty());

        var persistedReservation = reservationRepository.save(reservation);
        var reservationOptByPersonId = reservationRepository
                .findReservationByPersonId(persistedPerson1.getId());

        assertAll(
                () -> assertTrue(reservationOptByPersonId.isPresent()),
                () -> assertEquals(reservationOptByPersonId.get(), persistedReservation)
        );

        reservationRepository.deleteReservationByPersonId(persistedPerson1.getId());

        assertTrue(reservationRepository
                .findReservationByPersonId(persistedPerson1.getId())
                .isEmpty());
    }
}