package pl.novak.GetParked.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;
import pl.novak.GetParked.model.Person;
import pl.novak.GetParked.model.Queue;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
@Sql(statements = "DELETE FROM queue", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class QueueRepositoryTest {
    @Autowired
    QueueRepository queueRepository;
    @Autowired
    PersonRepository personRepository;
    Person person1;

    @BeforeEach
    void initializeVariables() {
        person1 = Person.builder()
                .firstName("person1")
                .lastName("lastname1")
                .email("email1")
                .login("login1")
                .password("pass1")
                .banned(false)
                .build();
    }

    @Test
    @DisplayName("Should return an optional with Queue for an existing ")
    void findByPersonId() {
        var persistedPerson = personRepository.save(person1);
        var persistedQueue = queueRepository
                .save(Queue.builder()
                        .person(persistedPerson)
                        .build());
        var retrievedQueue = queueRepository.findByPersonId(persistedPerson.getId());
        assertAll(
                () -> assertTrue(retrievedQueue.isPresent()),
                () -> assertEquals(retrievedQueue.get(), persistedQueue),
                () -> assertTrue(queueRepository
                        .findByPersonId(persistedPerson.getId() + 1L)
                        .isEmpty())
        );
    }
}