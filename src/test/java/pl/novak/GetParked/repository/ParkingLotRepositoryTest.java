package pl.novak.GetParked.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;
import pl.novak.GetParked.model.ParkingLot;
import pl.novak.GetParked.model.Person;
import pl.novak.GetParked.model.Reservation;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
@Sql(statements = "delete  from person; " +
        "delete from parkinglot; delete " +
        "from reservation",executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class ParkingLotRepositoryTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    ParkingLotRepository parkingLotRepository;
    @Autowired
    QueueRepository queueRepository;
    @Autowired
    ReservationRepository reservationRepository;

    Person person1;
    Person person2;
    ParkingLot parkingLot1;
    ParkingLot parkingLot2;

    @BeforeEach
    void initializeVariables() {
        person1 = Person.builder()
                .firstName("person1")
                .lastName("lastname1")
                .email("email1")
                .login("login1")
                .password("pass1")
                .banned(false)
                .build();
        person2 = Person.builder()
                .firstName("person2")
                .lastName("lastname2")
                .email("email2")
                .login("login2")
                .password("pass2")
                .banned(false)
                .build();
        parkingLot1 = ParkingLot.builder()
                .blockId("b1")
                .lotNumber(1)
                .available(true)
                .build();
        parkingLot2 = ParkingLot.builder()
                .blockId("b2")
                .lotNumber(2)
                .available(true)
                .build();
    }

    @Test
    @DisplayName("Should return an optional of a parking lot where owner is not null")
    void findFirstByAvailableTrueAndCurrentReservationIsNull_case1() {
        var persistedPerson1 = personRepository.save(person1);
        var persistedParkingLot1 = parkingLotRepository.save(parkingLot1);
        persistedParkingLot1.setOwner(persistedPerson1);
        persistedPerson1.setAssignedLot(persistedParkingLot1);
        var lotOptBeforeReservation = parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNull();
        assertAll(
                () -> assertTrue(lotOptBeforeReservation.isPresent()),
                () -> assertEquals(lotOptBeforeReservation.get(), persistedParkingLot1)

        );

        //setting the only available lot to unavailable
        persistedParkingLot1.setAvailable(false);
        assertTrue(parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNull()
                .isEmpty());

        //should return a next available lot after set the 1st available lot to unavailable
        var persistedParkingLot2 = parkingLotRepository.save(parkingLot2);
        var lotOptSetUnavailable = parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNull();
        assertAll(
                () -> assertTrue(lotOptSetUnavailable.isPresent()),
                () -> assertEquals(lotOptSetUnavailable.get(), persistedParkingLot2)

        );
    }

    @Test
    @DisplayName("Should return an optional of a parking lot where owner is not null")
    void findFirstByAvailableTrueAndCurrentReservationIsNull_case2() {
        var persistedPerson1 = personRepository.save(person1);
        var persistedParkingLot1 = parkingLotRepository.save(parkingLot1);
        persistedParkingLot1.setOwner(persistedPerson1);
        persistedPerson1.setAssignedLot(persistedParkingLot1);
        var lotOptBeforeReservation = parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNull();
        assertAll(
                () -> assertTrue(lotOptBeforeReservation.isPresent()),
                () -> assertEquals(lotOptBeforeReservation.get(), persistedParkingLot1)
        );
        //creating a reservation for the available lot
        persistedParkingLot1.setAvailable(true);
        var reservation = Reservation.builder()
                .parkingLot(persistedParkingLot1)
                .person(persistedPerson1)
                .build();
        var persistedReservation = reservationRepository.save(reservation);
        persistedParkingLot1.setCurrentReservation(persistedReservation);
        assertTrue(parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNull()
                .isEmpty());

        //should return a next available lot after reservation of the 1st
        var persistedParkingLot2 = parkingLotRepository.save(parkingLot2);
        var lotOptSetUnavailable = parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNull();
        assertAll(
                () -> assertTrue(lotOptSetUnavailable.isPresent()),
                () -> assertEquals(lotOptSetUnavailable.get(), persistedParkingLot2)
        );
    }

    @Test
    @DisplayName("Should return an optional of available lot where owner is null")
    void findFirstByAvailableTrueAndCurrentReservationIsNullAndOwnerIsNull_case1() {
        var persistedPerson1 = personRepository.save(person1);
        var persistedParkingLot1 = parkingLotRepository.save(parkingLot1);
        persistedParkingLot1.setOwner(persistedPerson1);
        persistedPerson1.setAssignedLot(persistedParkingLot1);
        var lotOptShouldBeEmpty = parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNullAndOwnerIsNull();
        assertTrue(lotOptShouldBeEmpty.isEmpty());

        var persistedParkingLot2 = parkingLotRepository.save(parkingLot2);

        var lotOptShouldBePresent = parkingLotRepository
                .findFirstByAvailableTrueAndCurrentReservationIsNullAndOwnerIsNull();
        assertAll(
                () -> assertTrue(lotOptShouldBePresent.isPresent()),
                () -> assertEquals(lotOptShouldBePresent.get(), persistedParkingLot2)
        );
    }

    @Test
    void name() {
        var persistedPerson1 = personRepository.save(person1);
        var persistedPerson2 = personRepository.save(person2);
        var persistedParkingLot1 = parkingLotRepository.save(parkingLot1);
        var persistedParkingLot2 = parkingLotRepository.save(parkingLot2);
        persistedParkingLot2.setOwner(persistedPerson2);
        persistedPerson2.setAssignedLot(persistedParkingLot2);

        System.err.println(parkingLotRepository.findFirstByAvailableTrueAndCurrentReservationIsNull());

        System.err.println(parkingLotRepository.findByOwnerIdAndAvailableTrueAndCurrentReservationIsNull(persistedPerson2.getId()));
    }
}
